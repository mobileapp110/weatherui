import 'package:flutter/material.dart';

void main() {
  runApp(const WeatherUI());
}

class WeatherUI extends StatelessWidget {
  const WeatherUI({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'UI: Weather',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        brightness: Brightness.light,
        iconTheme:
            const IconThemeData(color: Color.fromARGB(255, 255, 208, 52)),
        dividerTheme: const DividerThemeData(
            color: Colors.white, indent: 10, endIndent: 10),
      ),
      home: Scaffold(
        backgroundColor: const Color.fromARGB(255, 108, 160, 237),
        body: Stack(
          //ListView
          children: [
            Container(
              width: double.infinity,
              height: double.infinity,
              decoration: const BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage(
                      "https://www.guidingtech.com/wp-content/uploads/iOS-16-Weather-Wallpaper-2.jpg",
                    ),
                    fit: BoxFit.cover),
              ),
            ),
            SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    padding: const EdgeInsets.fromLTRB(10.0, 50.0, 10.0, 5.0),
                    child: const Text(
                      "Amphoe Mueang Chon Buri",
                      style: TextStyle(
                          fontSize: 26,
                          color: Colors.white,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 5.0),
                    child: const Text(
                      "32˚",
                      style: TextStyle(
                          fontSize: 70,
                          color: Colors.white,
                          fontWeight: FontWeight.w200),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 5.0),
                    child: const Text(
                      "Mostly Sunny",
                      style: TextStyle(
                          fontSize: 20,
                          color: Colors.white,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 30.0),
                    child: const Text(
                      "H:32˚ L:21˚",
                      style: TextStyle(
                          fontSize: 20,
                          color: Colors.white,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.all(10.0),
                    child: buildCard1(),
                  ),
                  Container(
                    padding: const EdgeInsets.all(10.0),
                    child: buildCard2(),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

Widget buildCard1() {
  return SizedBox(
    height: 180,
    child: Card(
      color: const Color.fromARGB(255, 77, 136, 238),
      child: Column(
        children: [
          const ListTile(
            title: Text(
              "Sunny condition will continue for the rest of the day. Wind gusts are up to 19km/h.",
              style: TextStyle(color: Colors.white),
            ),
          ),
          const Divider(),
          buildWeather(),
        ],
      ),
    ),
  );
}

Widget buildWeather() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      Column(
        children: const <Widget>[
          Text(
            "Now\n",
            style: TextStyle(color: Colors.white),
          ),
          Icon(Icons.sunny),
          Text(
            "\n32˚",
            style: TextStyle(color: Colors.white),
          ),
        ],
      ),
      Column(
        children: const <Widget>[
          Text(
            "15\n",
            style: TextStyle(color: Colors.white),
          ),
          Icon(Icons.sunny),
          Text(
            "\n32˚",
            style: TextStyle(color: Colors.white),
          ),
        ],
      ),
      Column(
        children: const <Widget>[
          Text(
            "16\n",
            style: TextStyle(color: Colors.white),
          ),
          Icon(Icons.sunny),
          Text(
            "\n31˚",
            style: TextStyle(color: Colors.white),
          ),
        ],
      ),
      Column(
        children: const <Widget>[
          Text(
            "17\n",
            style: TextStyle(color: Colors.white),
          ),
          Icon(Icons.sunny),
          Text(
            "\n30˚",
            style: TextStyle(color: Colors.white),
          ),
        ],
      ),
      Column(
        children: const <Widget>[
          Text(
            "17:55\n",
            style: TextStyle(color: Colors.white),
          ),
          Icon(Icons.wb_twighlight),
          Text(
            "\nSunset",
            style: TextStyle(color: Colors.white),
          ),
        ],
      ),
    ],
  );
}

Widget buildCard2() {
  return SizedBox(
    // height: 500, //800
    child: Card(
      color: Color.fromARGB(133, 97, 155, 255),
      child: Column(
        children: const [
          ListTile(
            leading: Icon(Icons.calendar_month_outlined, color: Colors.white),
            title: Text(
              "10-DAY FORECAST",
              style: TextStyle(color: Colors.white),
            ),
          ),
          Divider(),
          ListTile(
            title: Icon(Icons.sunny),
            leading: Text(
              "Today",
              style: TextStyle(color: Colors.white),
            ),
          ),
          Divider(),
          ListTile(
            leading: Text(
              "Sat",
              style: TextStyle(color: Colors.white),
            ),
            title: Icon(Icons.sunny),
          ),
          Divider(),
          ListTile(
            leading: Text(
              "Sun",
              style: TextStyle(color: Colors.white),
            ),
            title: Icon(Icons.cloud),
          ),
          Divider(),
          ListTile(
            leading: Text(
              "Mon",
              style: TextStyle(color: Colors.white),
            ),
            title: Icon(Icons.sunny),
          ),
          Divider(),
          ListTile(
            leading: Text(
              "Tue",
              style: TextStyle(color: Colors.white),
            ),
            title: Icon(Icons.cloud),
          ),
          Divider(),
          ListTile(
            leading: Text(
              "Wed",
              style: TextStyle(color: Colors.white),
            ),
            title: Icon(Icons.sunny),
          ),
          Divider(),
          ListTile(
            leading: Text(
              "Thu",
              style: TextStyle(color: Colors.white),
            ),
            title: Icon(Icons.sunny),
          ),
          Divider(),
          ListTile(
            leading: Text(
              "Fri",
              style: TextStyle(color: Colors.white),
            ),
            title: Icon(Icons.cloud),
          ),
          Divider(),
          ListTile(
            leading: Text(
              "Sat",
              style: TextStyle(color: Colors.white),
            ),
            title: Icon(Icons.cloud),
          ),
          Divider(),
          ListTile(
            leading: Text(
              "Sun",
              style: TextStyle(color: Colors.white),
            ),
            title: Icon(Icons.sunny),
          ),
        ],
      ),
    ),
  );
}
